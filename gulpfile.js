'use strict';

var gulp 	 	= require('gulp'),
	notify 		= require('gulp-notify'),
	cleanCSS 	= require('gulp-clean-css'),
	watch 		= require('gulp-watch'),
	rename 		= require('gulp-rename'),
	sass			= require('gulp-sass');

var error_obj = {
	title	: "CSS",
	subtitle: "Failure!",
	message	: "Error: <%= error.message %>",
	sound	: "Sosumi"
};

var success_obj = {
	title	: "CSS",
	subtitle: "Success!",
	message	: "File generated!",
	sound	: "Beep"
};

var config = {
	file_type	: '*.scss',
	source_file	: 'application.scss',
	source_path 	: 'assets/css/',
	destination_path: '',
	final_filename	: 'style.css'
};

gulp.task('default', ['css'], function() {
	gulp.watch(config.source_path + "**/" + config.file_type, ['css']);
});

gulp.task('css', function() {

	var onError = function(err) {
		notify.onError(error_obj)(err);
		this.emit('end');
	};

	gulp.src(config.source_path + config.source_file)
		.pipe(sass().on('error', onError))
		.pipe(cleanCSS({compatibility: 'ie8'}).on('error', onError))
		.pipe(rename(config.final_filename))
		.pipe(gulp.dest(config.destination_path))
		.pipe(notify(success_obj));;
});
