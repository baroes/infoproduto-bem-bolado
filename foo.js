function abTestConfirmadoLinguagemTata() {
    var attribute = "ab_bb1_home_optbtn";
    var variants = ["variant_a", "variant_b"];
    var variant = readCookie(attribute);
    if(variant == null) {
      variant = generateVariant(attribute, variants);
    }
    if(variant == variants[0]) {
    } else if(variant == variants[1]) {
      document.getElementById("btn-home-optin").classList.remove("non-pulse-button");
      document.getElementById("btn-home-optin").classList.add("pulse-button");
      document.getElementById("messenger").href="https://m.me/guiadacozinha?ref=w681121";
    }
    woopra.identify(attribute, variant);
  }